package com.revelation.wurm;

import com.wurmonline.server.creatures.Communicator;
import org.gotti.wurmunlimited.modloader.interfaces.*;
import org.gotti.wurmunlimited.modloader.interfaces.ServerStartedListener;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class VoteReward implements WurmServerMod, Configurable, ServerStartedListener, PlayerMessageListener {
	public static final String version = "ty1.2";
	private static final Logger logger = Logger.getLogger(VoteReward.class.getName());
	public static String APIKEY = "";
	public static int rewardAmt = 0;
	public static String rewardMsg = "";
	public static String claimedMsg = "";

	public void configure(Properties properties){
		VoteReward.APIKEY = properties.getProperty("APIKEY", APIKEY);
		VoteReward.rewardAmt = Integer.parseInt(properties.getProperty("rewardamt", String.valueOf(rewardAmt)));
		VoteReward.rewardMsg = properties.getProperty("rewardmsg", rewardMsg);
		VoteReward.claimedMsg = properties.getProperty("claimedmsg", claimedMsg);
		if(VoteReward.APIKEY.length() > 5) {
			VoteReward.logger.log(Level.INFO, "API KEY = " + VoteReward.APIKEY.substring(0, 5) + "...");
		} else {
			VoteReward.logger.log(Level.INFO, "API KEY = " + VoteReward.APIKEY);
		}
		VoteReward.logger.log(Level.INFO, "Reward Amount = " + VoteReward.rewardAmt);
		VoteReward.logger.log(Level.INFO, "Reward Message = " + VoteReward.rewardMsg);
		VoteReward.logger.log(Level.INFO, "Claimed Message= " + VoteReward.claimedMsg);
	}

	public static void logException(String msg, Throwable e) {
		if (logger != null)
			logger.log(Level.SEVERE, msg, e);
	}

	public static void logWarning(String msg) {
		if (logger != null)
			logger.log(Level.WARNING, msg);
	}

	public static void logInfo(String msg) {
		if (logger != null)
			logger.log(Level.INFO, msg);
	}

	@Override
	public void onServerStarted() {
		VoteReward.logger.info("Vote Rewards Enabled");
	}

	@Override
	public MessagePolicy onPlayerMessage(Communicator communicator, String message, String title) {
		if (message.startsWith("/vote")) {
			return VoteCommand.onPlayerMessage(communicator, message) ? MessagePolicy.DISCARD : MessagePolicy.PASS;
		}
		return MessagePolicy.PASS;
	}

	@Deprecated
	@Override
	public boolean onPlayerMessage(Communicator communicator, String msg) {
		return false;
	}

	@Override
	public String getVersion(){
		return version;
	}
}
